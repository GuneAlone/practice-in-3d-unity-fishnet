using UnityEngine;
public class playeroffline : MonoBehaviour
{
    private Animator animator;
    private Rigidbody playerRigidbody;
    public float speed = 5.0f;
    public float rotationSpeed = 10f;
    void Start()
    {
        animator = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }
    void Update()
    {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");

            Vector3 directionalVector = new Vector3(h, 0, v);

            if (directionalVector.magnitude > Mathf.Abs(0.0f))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(directionalVector), Time.deltaTime * 10);
                animator.SetFloat("speed", Vector3.ClampMagnitude(directionalVector, 1).magnitude);
                playerRigidbody.velocity = Vector3.ClampMagnitude(directionalVector, 1) * speed;
            }
    }
}