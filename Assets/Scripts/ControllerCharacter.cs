using UnityEngine;
using FishNet.Object;
using Cinemachine;

public class player : NetworkBehaviour
{
    private Animator animator;
    private Rigidbody playerRigidbody;
    public CinemachineFreeLook FL;
    public float speed = 5f;
    public float rotationSpeed = 10f; 

    void Update() 
    {
        animator = GetComponent<Animator>(); // �������� ���������, ������ ��� ���������� ����������
        playerRigidbody = GetComponent<Rigidbody>(); // �������� ���������, ������ ��� ���������� ����������

        if (!NetworkObject.IsOwner) // ��������, �������� �� �������� ��������� �������
        {
            FL.Priority = 0; // ���� �������� �� ���������, �� �������� ��������� ������ ����
            return; // ��� �� ��������� ������ ����, ��� ��� ���������� ��� ����� ���� ��� ���������� ���������, ����� ������� ����� ��������� ����� ����������� ������������
        }
        else
        {
            FL.Priority = 1; // ����� ��������� 1, ��� �� ������ ������� �� ��������� ���������� 
        }

        //����� ���� ���������� ����������

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector3 moveDirection = new Vector3(h, 0, v);

        if (moveDirection.magnitude > Mathf.Abs(0.0f))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(moveDirection), Time.deltaTime * 10);
            animator.SetFloat("speed", Vector3.ClampMagnitude(moveDirection, 1).magnitude);
            playerRigidbody.velocity = Vector3.ClampMagnitude(moveDirection, 1) * speed;
        }
    }
}